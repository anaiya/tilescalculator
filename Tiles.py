'''
Question
Find Cost of Tile to Cover W x H Floor - Calculate the total cost
of tile it would take to cover a floor plan of width and height, using a cost entered by the user

My interpretation of the problem is to create different tiles and different prices so that the customers can choose the
tile they like. After the input the tile of their choice, then they shall be prompted to provide the height and width of each
surface(wall or floor) they like to cover.

The input shall be integers.
'''

from Classes import *

jack = Customer('Jack', 500, 400)


#tile1 = Tile('Amethyst', 640)
#tile2 = Tile('Ruby', 1200)
#tile3 = Tile('Amber', 590)


#jack.calculate_price(jack.price,jack.area)

cat1name = input('What is your name?\n')
cat1price = int(input('Which tile did you choose?\n'))
cat1area = int(input('what is the are that we are covering?\n'))

cat1 = Customer(cat1name, cat1price, cat1area)

cat1.calculate_price(cat1price, cat1area)

#cat1.calculate_price(tile1.price, cat1.area)